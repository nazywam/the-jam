package ;


import flixel.tweens.FlxTween;
import flixel.effects.particles.FlxParticle;
import flixel.FlxCamera.FlxCameraFollowStyle;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxBar;
import flixel.util.FlxTimer;
import openfl.Assets;
import flixel.tile.FlxTile;
import flixel.input.keyboard.FlxKey;
import flixel.input.FlxInput.FlxInputState;
import openfl.events.KeyboardEvent;
import flixel.tweens.FlxEase;
class PlayState extends FlxState {
	
	var player : Player;
	var playerSpawn : Doors;
	var playerExit : Doors;
	
	var map : FlxTilemap;
	var backgroundTiles : FlxTilemap;
	var foregroundTiles : FlxTilemap;
	
	var back : FlxGroup;
	var mid : FlxGroup;
	var front : FlxGroup;
	
	var teleports : FlxGroup;
	var ammos : FlxGroup;
	var terminals : FlxGroup;
	var spikes : FlxGroup;
	var lasers : FlxGroup;
	var platforms : FlxGroup;
	var turrets : FlxGroup;
	
	var enemies : FlxGroup;
	
	var stopers : FlxGroup;
	
	var paused : Bool = false;

	var ammoBar : FlxBar;
	var ammoBarShake : FlxTween;
	var ammoBarShakeP : Array<FlxPoint>;
	var upgradeBar : FlxBar;
	
	var props : FlxGroup;
	
	var plan1 : FlxGroup;
	var plan2 : FlxGroup;
	var plan3 : FlxGroup;
	
	var terminalOpened : Bool = false;
	
	var minigame : Minigame;
	var minigameOpened : Bool = false;
	
	
	
	override public function create() {
		super.create();
		FlxG.mouse.visible = false;
		//FlxG.camera.followLerp = 60;
		
		back = new FlxGroup();
		mid = new FlxGroup();
		front = new FlxGroup();
		
		terminals = new FlxGroup();
		mid.add(terminals);
		
		teleports = new FlxGroup();
		mid.add(teleports);
		spikes = new FlxGroup();
		mid.add(spikes);
		ammos = new FlxGroup();
		mid.add(ammos);
		lasers = new FlxGroup();
		mid.add(lasers);
		platforms = new FlxGroup();
		mid.add(platforms);
		turrets = new FlxGroup();
		mid.add(turrets);
		
		enemies = new FlxGroup();
		mid.add(enemies);
		
		
		stopers = new FlxGroup();
		back.add(stopers);
		
		map = new FlxTilemap();
		backgroundTiles = new FlxTilemap();
		foregroundTiles = new FlxTilemap();
		loadLevel(Assets.getText("assets/data/level3.txt"));

		if (Reg.world == 1) {						
			var background = new FlxTilemap();
			background.loadMap(Assets.getText("assets/data/world1Background.txt"), "assets/sprites/world1Background.png", 128, 128, 1, 1);
			back.add(background);
		} else if (Reg.world == 2) {
			var background = new FlxSprite(0, 0, "assets/sprites/background/background.png");
			background.scrollFactor.set(0, 0);
			add(background);
			generateBackground();
		}
		
		
		back.add(backgroundTiles);
		front.add(foregroundTiles);
		back.add(map);
		
		props = new FlxGroup();
		
		if (Reg.world == 2) {
			var floor = new FlxSprite(0, map.height - 10, "assets/sprites/world2Floor.png");
			floor.y -= floor.height;
			props.add(floor);
			
			if (Reg.level == 1) {
				var start = new FlxSprite(0, map.height -10, "assets/sprites/world2Start.png");
				start.y -= start.height + floor.height + 3;	
				props.add(start);
				playerSpawn.x += 2;
			}			
			mid.add(props);
		}
		
		
		add(back);
		add(mid);
		add(front);
		
		ammoBar = new FlxBar(FlxG.camera.width - 5, FlxG.camera.height - 5, FlxBarFillDirection.LEFT_TO_RIGHT, 50, 10, Reg, "weaponAmmo",0, 16);
		ammoBar.createImageBar("assets/sprites/ammoBarEmpty.png", "assets/sprites/ammoBarFilled.png");
		ammoBar.x -= ammoBar.width;
		ammoBar.y -= ammoBar.height;
		ammoBar.scrollFactor.y = ammoBar.scrollFactor.x = 0;
		ammoBar.setCallbacks(null , upgradeWeapon, false);
		front.add(ammoBar);
	
		ammoBarShakeP = new Array<FlxPoint>();
		ammoBarShakeP.push(new FlxPoint(ammoBar.x, ammoBar.y));
		ammoBarShakeP.push(new FlxPoint(ammoBar.x + 1, ammoBar.y));
		ammoBarShakeP.push(new FlxPoint(ammoBar.x, ammoBar.y));
		ammoBarShake = FlxTween.linearPath(ammoBar, ammoBarShakeP, .2, true, {ease : FlxEase.bounceOut } );
		
		upgradeBar = new FlxBar(5, FlxG.camera.height - 5, FlxBarFillDirection.LEFT_TO_RIGHT, 50, 10, Reg, "weaponUpgradeLevel", 0, 5);
		upgradeBar.createImageBar("assets/sprites/upgradeBarEmpty.png", "assets/sprites/upgradeBarFilled.png");
		upgradeBar.y -= upgradeBar.height;
		upgradeBar.scrollFactor.x = upgradeBar.scrollFactor.y = 0;
		front.add(upgradeBar);
		
		FlxG.camera.follow(player, FlxCameraFollowStyle.PLATFORMER);
		FlxG.worldBounds.set(0, 0, map.width, map.height);
		FlxG.camera.setScrollBoundsRect(0, 0, map.width - 16, map.height - 48);
		FlxG.camera.pixelPerfectRender = false;
		FlxG.camera.targetOffset.y = -32;
	}
	public function generateBackground() {
		plan3 = new FlxGroup();
		add(plan3);
		plan2 = new FlxGroup();
		add(plan2);
		plan1 = new FlxGroup();
		add(plan1);
		
		var previousWidth = .0;
		var widthSum = .0;
		var previousBlockID = -1;
		
		while (true) {
			var blockID = Std.random(13);
			if (blockID == previousBlockID) {
				blockID = (blockID + 1) % 13;
			}
			previousBlockID = blockID;
			
			var s = new FlxSprite(previousWidth, FlxG.height, "assets/sprites/background/plan3-" + blockID + ".png");
			s.scrollFactor.x = 0.07;
			s.scrollFactor.y = 0.02;
			s.y -= s.height + 56;
			previousWidth += s.width;
			widthSum += s.width;
			plan3.add(s);
			if (widthSum >= 1000) {
				break;
			}
			previousWidth--;
		}
		
		previousWidth = .0;
		widthSum = .0;
		previousBlockID = -1;
		
		while (true) {
			var blockID = Std.random(5);
			if (blockID == previousBlockID) {
				blockID = (blockID + 1) % 5;
			}
			previousBlockID = blockID;
			var s = new FlxSprite(previousWidth, FlxG.height, "assets/sprites/background/plan2-" + blockID + ".png");
			s.scrollFactor.x = .1;
			s.scrollFactor.y = .02;
			s.y -= s.height + 56;
			previousWidth += s.width;
			widthSum += s.width;
			plan2.add(s);
			if (widthSum >= 1000) {
				break;
			}
		}
		
		previousWidth = .0;
		widthSum = .0;
		previousBlockID = -1;
		
		var tileCount = 0;
		
		while (true) {
			
			var blockID = Std.random(8);
			if (tileCount % 2 == 0) {
				blockID = 5;
			}
			
			var s = new FlxSprite(previousWidth, 775, "assets/sprites/background/plan1-" + blockID + ".png");
			previousWidth--;
			s.scrollFactor.x = 1;
			s.scrollFactor.y = 1;
			s.y -= s.height;
			previousWidth += s.width;
			widthSum += s.width;
			plan1.add(s);
			
			previousBlockID = blockID;
			tileCount++;
			if (widthSum >= map.width) {
				break;
			}
		}
		
	}
	public function spawnPlayer() {
		player = new Player(playerSpawn.x, playerSpawn.y);
		player.y += playerSpawn.height - player.height;
		player.x += 5;
		FlxG.camera.follow(player);
		playerSpawn.animation.play("start");
		new FlxTimer(0.77, function(_) {
			mid.add(player);
			front.add(player.bullets);
			mid.add(player.gibs);
		});
	}
	public function enterTeleport(player : Player, teleport : Teleport) {
		if (teleport.entrance && FlxG.keys.justPressed.DOWN && !player.freezed) {
			for (x in teleports) {
				var t = cast(x, Teleport);
				if (!t.entrance && t.id == teleport.id) {
					FlxG.camera.followLerp = 0.1;
					player.freezed = true;
					player.solid = false;
					player.animation.play("teleportOut");
					new FlxTimer(0.6, function foo(_) {
						player.y = t.y + t.height - player.height;
						player.x = t.x + t.width / 2 - player.width / 2 + 1;
						player.animation.play("teleportIn");
						new FlxTimer(0.6, function foor(_) { player.freezed = false; player.solid = true; FlxG.camera.followLerp = 60;} );
					});
				}
			}
		}
	}
	public function upgradeWeapon() {
		player.weaponUpgradeProgress = 0;
	}
	public function collectAmmo(player : Player, ammo : Ammo) {
		ammo.destroy();
		ammos.remove(ammo);
		Reg.weaponAmmo += 4;
		shakeAmmoBar();
	}
	public function bulletHitActor(bullet : Bullet, actor : Actor) {
		
		hitActor(actor, bullet.velocity.x/2, -20);
		bullet.piericingPower--;
		
		if (bullet.piericingPower <= 0) {
			bullet.destroy();
			bullet.alpha = 0;
			bullet.solid = false;
			if (Std.is(actor, Enemy)) {	
				player.bullets.remove(bullet);
			}
		}
	}
	public function openTerminal(_, terminal : Terminal) {
		if (FlxG.keys.justPressed.DOWN) {
			minigame = new Minigame(FlxG.camera.scroll.x, FlxG.camera.scroll.y, terminal.opens);
			front.add(minigame);
			minigameOpened = true;
			paused = true;
		}
	}
	public function hitActor(actor : Actor, speedX : Float, speedY : Float) {
		paused = true;
		var a = new FlxTimer(0.02, function foo(f : FlxTimer) { paused = false; } );
		FlxG.camera.shake(0.007, 0.123);
		
		actor.health --;
		
		actor.gibs.velocity.set((speedX-50), (speedY-50), (speedX+50), (speedY+50));
		actor.gibs.angularVelocity.set( 0, 150*2, 0, 150*2);
		actor.die();
		
	}
	public function destroyBullet(bullet : Bullet, _) {
		bullet.animation.play("hit");
		bullet.solid = false;
		bullet.velocity.x = 0;
	}
	public function collectGibs(player : Player, p : FlxParticle) {
		p.visible = false;
		player.weaponUpgradeProgress++;
		p.lifespan = 0;
	}
	public function distance(x1 : Float, y1 : Float, x2 : Float, y2 : Float) {
		return(Math.sqrt( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) ));
	}
	public function shakeAmmoBar() {
		ammoBarShake = FlxTween.linearPath(ammoBar, ammoBarShakeP, .2, true, {ease : FlxEase.bounceInOut } );
	}
	override public function update(elapsed : Float) {
	
		if (minigameOpened) {	
			minigame.update(elapsed);
			if (minigame.visible == false) {
				if (minigame.succes) {
					for (x in lasers) {
						if (cast(x, Laser).openedBy == minigame.opens) {
							cast(x, Laser).turnOff();
						}
					}
					for (x in platforms) {
						if (cast(x, Platform).startedBy == minigame.opens) {
							cast(x, Platform).moving = true;
						}
					}
				}
				paused = false;
				minigameOpened = false;
				minigame.destroy();
			}
		}
		if (paused) return;
		
		for (x in enemies) {
			var e = cast(x, Enemy);
			if (!e.dead) {	
				
				if (!e.isShooting) {
					if ((player.y + player.height > e.y && player.y < e.y + e.height && e.hasWeapon && 
							(player.x < e.x && !e.turnedRight || player.x > e.x && e.turnedRight)) && Math.abs(e.x - player.x) < 150) {
						e.isShooting = true;
						new FlxTimer(.5, function(_) {
							if (!e.dead) {
								e.isShooting = false;
								e.fire();	
							}
						});
					} else {
						if (!e.turnedRight) {
							e.velocity.x = -e.speed;
						} else {
							e.velocity.x = e.speed;
						}
					}
				}
			}
			FlxG.collide(e.bullets, map, function(b : Bullet, _) {
				b.animation.play("hit");
			});
			FlxG.overlap(e.bullets, player, bulletHitActor);
			if (e.dead) {
				FlxG.collide(cast(x, Enemy).gibs, map);
				for (q in cast(x, Enemy).gibs) {
					FlxG.overlap(player, cast(q, FlxParticle), collectGibs);
				}
			}
			FlxG.overlap(player, e, function(player : Player, enemy) {
				e.isPunching = true;
				e.animation.play("punch");
				var direction = FlxObject.LEFT;
				if (e.turnedRight) FlxObject.RIGHT;
				
				new FlxTimer(0.06, function(_) {
					hitActor(player, e.velocity.x, e.velocity.y);	
				});
				
				new FlxTimer(0.1, function(_) {
					e.isPunching = false;
				});
			});
		}
		
		if (FlxG.keys.pressed.ESCAPE) {
			#if desktop
			Sys.exit(0);
			#end
		}
		
		if (FlxG.keys.justPressed.F5) {
			mid.remove(player);
			front.remove(player.bullets);
			mid.remove(player.gibs);
			spawnPlayer();
		}
		
		player.x = Math.round(player.x);
		
		for (x in turrets) {
			var t = cast(x, Turret);
			FlxG.collide(t.bullets, map, destroyBullet);
			FlxG.overlap(t.bullets, player, bulletHitActor);
		}
		
		FlxG.overlap(player, terminals, openTerminal);
		FlxG.collide(enemies, map, function(enemy : Enemy, _) {
			if (enemy.isTouching(FlxObject.WALL)) {
				enemy.turnedRight = !enemy.turnedRight;
			}
		});
		FlxG.collide(enemies, stopers, function(enemy : Enemy, _) {
			enemy.turnedRight = !enemy.turnedRight;
		});
		FlxG.overlap(platforms, stopers, function(platform : Platform, _) {
			platform.moveDirection = -platform.moveDirection;
			platform.update(elapsed);
		});
		FlxG.overlap(player, playerExit, function(_, doors : Doors) {
			if (FlxG.keys.justPressed.DOWN && doors.animation.name != "finish") {
				player.visible = false;
				player.freezed = true;
				doors.animation.play("finish");	
			}
			if (doors.animation.name == "finish" && doors.animation.finished) {
				FlxG.camera.alpha -= 0.01;
			}
		});
		
	
		FlxG.collide(player.gibs, platforms);
		FlxG.collide(player, map);
		FlxG.collide(player, platforms, function(_, platform : Platform) {
			if (player.isTouching(FlxObject.DOWN) && platform.isTouching(FlxObject.DOWN)) {
				hitActor(player, 0, 50);
			}
		});
		FlxG.overlap(player, teleports, enterTeleport);
		FlxG.overlap(player, ammos, collectAmmo);
		FlxG.overlap(player.bullets, enemies, bulletHitActor);
		FlxG.collide(player.bullets, map, destroyBullet);
		FlxG.collide(player.gibs, map);
		FlxG.overlap(player, spikes, function(_, spike : Spikes) {
			if (player.velocity.y > 0 || cast(spike, Spikes).type == Spikes.SpikeType.Round) {
				hitActor(player, player.velocity.x, player.velocity.y);	
			}
		});
		
		FlxG.overlap(player, lasers, function(_, _) {
			hitActor(player, player.velocity.x, player.velocity.y);
		});
	
		if (FlxG.keys.justPressed.SPACE && player.animation.name != "front" && !player.freezed && !player.dead) {
			if (Reg.weaponAmmo > 0) {
				player.fire();						
				Reg.weaponAmmo--;
			} else {
				shakeAmmoBar();
			}
		}
		
		super.update(elapsed);
	}
	function loadLevel(level : String) {
		
		var items = level.split("[]");
		
		#if desktop
			map.loadMap(items[0].split("data=")[1], TestFeatures.artefactFix("assets/sprites/tiles.png", 16, 16), 16, 16, 1, 1);
			backgroundTiles.loadMap(items[0].split("data=")[2], TestFeatures.artefactFix("assets/sprites/tiles.png", 16, 16), 16, 16, 1, 1);
			foregroundTiles.loadMap(items[0].split("data=")[3], TestFeatures.artefactFix("assets/sprites/tiles.png", 16, 16), 16, 16, 1, 1);
		#else
			map.loadMap(items[0].split("data=")[1], "assets/sprites/tiles.png", 16, 16, 1, 1);
			backgroundTiles.loadMap(items[0].split("data=")[2], "assets/sprites/tiles.png", 16, 16, 1, 1);
			foregroundTiles.loadMap(items[0].split("data=")[3], "assets/sprites/tiles.png", 16, 16, 1, 1);
		#end
		
		for (x in 1...items.length) {
			var ids = items[x].substring(2, items[x].length - 3).split("\n");

			var entry = ids[0].split("=");
			var pos4 = ids[1].split("=")[1].substring(0, ids[1].split("=")[1].length - 3);
			var pos2 = pos4.split(",");

			var posX = Std.parseInt(pos2[0]);
			var posY = Std.parseInt(pos2[1]) -1;
			var type = entry[1].substring(0, entry[1].length -1);
			switch(type) {
				case "TeleportEntrance":
					var ID = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					teleports.add(new Teleport(posX*16, posY*16, Std.parseInt(ID), true));
				case "TeleportExit":
					var ID = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					teleports.add(new Teleport(posX * 16, posY * 16, Std.parseInt(ID), false));
				case "Ammo":
					ammos.add(new Ammo(posX * 16, posY * 16));
				case "Stoper":
					var a = new FlxSprite(posX * 16, posY * 16);
					a.width = a.height = 16;
					a.loadGraphic("assets/sprites/tiles.png", false, 16, 16);
					a.animation.add("default", [53]);
					a.animation.play("default");
					a.immovable = true;
					a.alpha = 0;
					stopers.add(a);
				case "PlayerSpawn":
					playerSpawn = new Doors(posX * 16 - 21, posY * 16 - 25, true);
					mid.add(playerSpawn);
					spawnPlayer();	
				case "Enemy":
					var hasWeapon = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var still = ids[3].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var turnedRight = ids[4].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var e = new Enemy(posX * 16, posY * 16, Std.parseInt(hasWeapon) == 1);
					e.y -= e.height - 16;
					
					
					e.turnedRight = Std.parseInt(turnedRight) == 1;
					if (Std.parseInt(still) == 1) {
						e.speed = 0;
					}
					
					enemies.add(e);
					front.add(e.bullets);
					mid.add(e.gibs);
				case "Terminal":
					var opens = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var t = new Terminal(posX * 16, posY * 16, Std.parseInt(opens));
					terminals.add(t);
				case "Spikes":
					var type = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					if (type == 'b') {
						var s = new Spikes(posX * 16, posY * 16, Spikes.SpikeType.Brick);
						spikes.add(s);
					} else {
						var s = new Spikes(posX * 16 + 4, posY * 16 + 3, Spikes.SpikeType.Round);
						spikes.add(s);
					}
				case "Laser":
					var ID = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var openedBy = ids[3].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					lasers.add(new Laser(posX * 16, posY * 16, Std.parseInt(ID), Std.parseInt(openedBy)));
				case "Platform":
					var moveType = Platform.MoveType.Static;
					var move = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var startedBy = ids[3].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					
					if (startedBy == '-') {
						startedBy = "-1";
					}
					
					switch(move) {
						case 'v':
							moveType = Platform.MoveType.Vertical;
						case 'h':
							moveType = Platform.MoveType.Horizontal;
						case 's':
							moveType = Platform.MoveType.Static;
					}
					platforms.add(new Platform(posX * 16, posY * 16, moveType, Std.parseInt(startedBy)));
				case "PlayerExit":
					playerExit = new Doors(posX * 16 -21, posY * 16 - 25, false);
					mid.add(playerExit);
				case "Turret":
					var p = ids[2].split("=")[1].substr(0, ids[2].split("=")[1].length - 1);
					var t =  new Turret(posX * 16, posY * 16, Std.parseInt(p));
					turrets.add(t);
					mid.add(t.bullets);
			}
		}
	}
}
