package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxRect;
import openfl.system.System;

class MenuState extends FlxState {
	
	
	var animations : FlxText;
	var start : FlxText;
	
	var startGame : FlxSprite;
	var help : FlxSprite;
	var credits : FlxSprite;
	
	var selection : FlxSprite;
	
	var background : FlxSprite;
	var moon : FlxSprite;
	var scrollTarget : Float = 0;
	
	override public function create() {
		super.create();
		FlxG.log.redirectTraces = true;
		FlxG.fixedTimestep = false;
		
		background = new FlxSprite(-427, 0, "assets/sprites/menu/background.png");
		add(background);
		
		startGame = new FlxSprite(FlxG.width / 2, 50);
		startGame.loadGraphic("assets/sprites/menu/startGame.png");
		startGame.x -= startGame.width / 2;
		add(startGame);
		
		help = new FlxSprite(FlxG.width / 2, 100);
		help.loadGraphic("assets/sprites/menu/help.png");
		help.x -= help.width / 2;
		add(help);
		
		credits = new FlxSprite(FlxG.width / 2, 150);
		credits.loadGraphic("assets/sprites/menu/credits.png");
		credits.x -= credits.width / 2;
		add(credits);
		
		selection = new FlxSprite(startGame.x, startGame.y, "assets/sprites/menu/selection.png");		
		selection.ID = 0;
		add(selection);
		
		animations = new FlxText(0, 0, 0, "Animacje");
		add(animations);
	}

	override public function update(elapsed : Float) {
		super.update(elapsed);
	
		if (FlxG.camera.scroll.x != scrollTarget) {
			FlxG.camera.scroll.x += (scrollTarget - FlxG.camera.scroll.x) / 10;
		}
		
		
		if (FlxG.keys.justPressed.UP) {
			if (selection.ID > 0) {
				selection.ID--;
				selection.y -= 50;
			}
		} else if (FlxG.keys.justPressed.DOWN) {
			if (selection.ID < 2) {
				selection.ID++;
				selection.y += 50;
			}
		}
		
		if (FlxG.keys.justPressed.ENTER) {
			
			FlxG.switchState(new PlayState());	
			scrollTarget = 427;
		}
		
		
		
		if (FlxG.mouse.justPressed) {
			if (new FlxPoint(FlxG.mouse.x, FlxG.mouse.y).inFlxRect(new FlxRect(animations.x, animations.y, animations.width, animations.height))) {
				
				FlxG.switchState(new Animations());
			} else {
				//
			}
			
		}
		
	}
}