package ;
import flixel.FlxSprite;

/**
 * ...
 * @author Michael
 */
class Platform extends FlxSprite{

	public function new(posX : Float, posY : Float) {
		super(posX : Float, posY : Float);
		loadGraphic("assets/sprites/platform.png", false, 48, 16);
	}
}