package ;

import flixel.FlxSprite;

/**
 * ...
 * @author Michael
 */
class Teleport extends FlxSprite
{
	public var id : Int;
	public var entrance : Bool;
	public function new(X:Float, Y:Float,  i : Int,  e : Bool) {
		super(X, Y);
		loadGraphic("assets/sprites/objects.png", false, 16, 16);
		id = i;
		entrance = e;
		
		if (entrance) {
			animation.add("default", [0,1], 2);
		} else {
			animation.add("default", [2]);
		}
		animation.play("default");
		
		width = 6;
		offset.x = 5;
		x += 5;
		
	}
	
}