package ;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxTimer;
/**
 * ...
 * @author Michael
 */
class Turret extends FlxSprite {
	
	public var bullets : FlxGroup;
	public var period : Int;
	
	public function new(posX : Float, posY : Float, p : Int) {
		super(posX, posY);
		period = p;
		loadGraphic("assets/sprites/objects.png", false, 16, 16);
		animation.add("default", [21]);
		animation.play("default");
		
		bullets = new FlxGroup();
		fire();
	}
	public function fire() {
		
		for (q in 0...2) {
			var a = new Bullet(x + 5 + q * 4, y + 3, true, 100, 1);
			a.angle = 90;
			a.velocity.y = 100;
			bullets.add(a);
			
			a.width = 1;
			a.offset.x = -1;
			a.height = 7;
			a.offset.y = 6;
		}
		
		new FlxTimer(period/100, function(_) {
			fire();
		});
	}
	override public function update(e : Float) {
		super.update(e);
		for (x in bullets) {
			var b = cast(x, Bullet);
			if (b.velocity.y == 0) {
				b.destroy();
				bullets.remove(b);
			}
		}
	}
}