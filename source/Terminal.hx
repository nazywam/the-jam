package ;
import flixel.FlxSprite;
import openfl.Assets;
/**
 * ...
 * @author Michael
 */
class Terminal extends FlxSprite {

	public var opens : Int = -1;
	
	public function new(posX : Float, posY : Float, opens : Int) {
		super(posX, posY);
		this.opens = opens;
		loadGraphic("assets/sprites/objects.png", false, 16, 16);
		animation.add("default", [3]);
		animation.play("default");
	}
	
}