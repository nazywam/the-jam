package ;
import flixel.FlxSprite;
/**
 * ...
 * @author Michael
 */

//1 - normal
//2 - long range
 
class Ammo extends FlxSprite {

	public function new(X : Float, Y : Float) {
		super(X, Y);
		loadGraphic("assets/sprites/objects.png", false, 16, 16);
		animation.add("default", [10]);
		animation.play("default");
	}
}