package ;
import flixel.FlxSprite;
/**
 * ...
 * @author Michael
 */
class Doors extends FlxSprite {

	var start : Bool;
	
	public function new(posX : Float, posY : Float, s : Bool) {
		super(posX, posY);
		start = s;
		loadGraphic("assets/sprites/doors.png", true, 37, 41);
		animation.add("start", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0], 10, false);
		animation.add("finish", [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], 10, false);
	}
	
}