package ;
import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**
 * ...
 * @author Michael
 */
class Laser extends FlxGroup {

	public var openedBy : Int = -1;
	var sprite : FlxSprite;
	
	public function new(posX : Float, posY : Float, t : Int, openedBy : Int) {
		super();
		
		this.openedBy = openedBy;
		
		sprite = new FlxSprite(posX, posY);
		sprite.loadGraphic("assets/sprites/lasers.png", false, 16, 48);
		add(sprite);
		sprite.animation.add("on", [6,5,4,3,2,1,0], 25, false);
		sprite.animation.add("off", [0, 1, 2, 3, 4, 5, 6], 25, false);
		sprite.animation.play("on");
		sprite.width = 8;
		sprite.offset.x = 4;
	}
	public function turnOff() {
		sprite.animation.play("off");
	}
	override public function update(e : Float) {
		super.update(e);
		if (sprite.animation.name == "off" && sprite.animation.finished) {
			sprite.solid = false;
		}
		
	}
	
}