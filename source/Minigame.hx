package ;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
/**
 * ...
 * @author Michael
 */
class Minigame extends FlxGroup {

	public var x : Float;
	public var y : Float;
	
	var background : FlxSprite;
	
	var scalingUp : Bool = true;
	var scalingDown : Bool = false;
	
	var screenSize : Int = 200;
	
	var pieces : FlxGroup;
	var board : String = "BBBBBBK#BB##B##C";	
	
	var won : Bool = false;
	public var opens : Int;
	
	public var succes : Bool = false;
	
	public function new(posX : Float, posY : Float, opens : Int) {
		super();
		this.opens = opens;
		
		background  = new FlxSprite(0, 0, "assets/sprites/minigame/background.png");
		background.scale.x = background .scale.y = 0;
		background.scrollFactor.x = background.scrollFactor.y = 0;
		add(background );
		
		pieces = new FlxGroup();
		add(pieces);
		
		for (y in 0...4) {
			for (x in 0...4) {
				if (board.charAt(y * 4 + x) != '#') {
					var type : MinigameTile.BlockType = MinigameTile.BlockType.Error;
					switch(board.charAt(y * 4 + x)) {
						case 'B':
							type = MinigameTile.BlockType.Block;
						case 'K':
							type = MinigameTile.BlockType.Key;
						case 'C':
							type = MinigameTile.BlockType.Chest;
						case 'S':
							type = MinigameTile.BlockType.Solid;
					}
					
					var a = new MinigameTile(114 + x * 51, 20 + y * 51, x, y, type);
					a.scrollFactor.x = a.scrollFactor.y = 0;
					a.scale.x = a.scale.y = 0;
					pieces.add(a);
				}
			}
		}		
	}
	
	public function moveHorizontaly(first : Int, second : Int, third : Int, fourth : Int, direction : Int) {
		for (x in pieces.members) {
					var t = cast(x, MinigameTile);
					if (t.gridX == second) {
						var canMove : Bool = true;
						for (q in pieces.members) {
							var other = cast(q, MinigameTile);
							if (other.gridX == first && other.gridY == t.gridY) {
								canMove = false;
								if (other.type == MinigameTile.BlockType.Key && t.type == MinigameTile.BlockType.Chest
								|| other.type == MinigameTile.BlockType.Chest && t.type == MinigameTile.BlockType.Key) {
									won = true;
									canMove = true;
								}
							}
						}
						if (canMove && t.type != MinigameTile.BlockType.Solid) {
							t.destination.x += 51 * direction;
							t.gridX += 1 * direction;
						}
					}
					if (t.gridX == third) {
						var canMove : Bool = true;
						for (q in pieces.members) {
							var other = cast(q, MinigameTile);
							if (other.gridX == second && other.gridY == t.gridY) {
								canMove = false;
								if (other.type == MinigameTile.BlockType.Key && t.type == MinigameTile.BlockType.Chest
								|| other.type == MinigameTile.BlockType.Chest && t.type == MinigameTile.BlockType.Key) {
									won = true;
									canMove = true;
								}
							}
						}
						if (canMove && t.type != MinigameTile.BlockType.Solid) {
							t.destination.x += 51 * direction;
							t.gridX += 1 * direction;
						}
					}
					if (t.gridX == fourth) {
						var canMove : Bool = true;
						for (q in pieces.members) {
							var other = cast(q, MinigameTile);
							if (other.gridX == third && other.gridY == t.gridY) {
								canMove = false;
								if (other.type == MinigameTile.BlockType.Key && t.type == MinigameTile.BlockType.Chest
								|| other.type == MinigameTile.BlockType.Chest && t.type == MinigameTile.BlockType.Key) {
									won = true;
									canMove = true;
								}
							}
						}
						if (canMove && t.type != MinigameTile.BlockType.Solid) {
							t.destination.x += 51 * direction;
							t.gridX += 1 * direction;
						}
					}
				}
	}
	
	public function moveVertically(first : Int, second : Int, third : Int, fourth : Int, direction : Int){
		for (x in pieces.members) {
					var t = cast(x, MinigameTile);
					if (t.gridY == second) {
						var canMove : Bool = true;
						for (q in pieces.members) {
							var other = cast(q, MinigameTile);
							if (other.gridY == first && other.gridX == t.gridX) {
								canMove = false;
								if (other.type == MinigameTile.BlockType.Key && t.type == MinigameTile.BlockType.Chest
								|| other.type == MinigameTile.BlockType.Chest && t.type == MinigameTile.BlockType.Key) {
									won = true;
									canMove = true;
								}
							}
						}
						if (canMove && t.type != MinigameTile.BlockType.Solid) {
							t.destination.y += 51 * direction;
							t.gridY += 1 * direction;
						}
					}
					if (t.gridY == third) {
						var canMove : Bool = true;
						for (q in pieces.members) {
							var other = cast(q, MinigameTile);
							if (other.gridY == second && other.gridX == t.gridX) {
								canMove = false;
								if (other.type == MinigameTile.BlockType.Key && t.type == MinigameTile.BlockType.Chest
								|| other.type == MinigameTile.BlockType.Chest && t.type == MinigameTile.BlockType.Key) {
									won = true;
									canMove = true;
								}
							}
						}
						if (canMove && t.type != MinigameTile.BlockType.Solid) {
							t.destination.y += 51 * direction;
							t.gridY+= 1 * direction;
						}
					}
					if (t.gridY == fourth) {
						var canMove : Bool = true;
						for (q in pieces.members) {
							var other = cast(q, MinigameTile);
							if (other.gridY == third && other.gridX == t.gridX) {
								canMove = false;
								if (other.type == MinigameTile.BlockType.Key && t.type == MinigameTile.BlockType.Chest
								|| other.type == MinigameTile.BlockType.Chest && t.type == MinigameTile.BlockType.Key) {
									won = true;
									canMove = true;
								}
							}
						}
						if (canMove && t.type != MinigameTile.BlockType.Solid) {
							t.destination.y +=51 * direction;
							t.gridY+= 1 * direction;
						}
					}
				}
	}
	
	override public function update(e : Float) {
		super.update(e);
		
		if (scalingUp) {
			background.scale.x = background.scale.x + (1 - background.scale.x) / 10;
			background.scale.y = background.scale.y + (1 - background.scale.y) / 10;
			for (x in pieces.members) {
				var t = cast(x, MinigameTile);
				t.scale.x = t.scale.x + (1 - t.scale.x) / 10;
				t.scale.y = t.scale.y + (1 - t.scale.y) / 10;
			}
			if (background.scale.x >= 0.99) {
				background.scale.x = background.scale.y = 1;
				scalingUp = false;
			}
		} else if (scalingDown) {
			background.scale.x = background.scale.x + (0 - background.scale.x) / 10;
			background.scale.y = background.scale.y + (0 - background.scale.y) / 10;
			
			for (x in pieces.members) {
				var t = cast(x, MinigameTile);
				t.scale.x = t.scale.x + (0 - t.scale.x) / 10;
				t.scale.y = t.scale.y + (0 - t.scale.y) / 10;
			}
			
			if (background.scale.x <= 0.01) {
				scalingDown = false;
				visible = false;
			}
		} else {
			if (won) {
				for (x in pieces.members) {
					if (cast(x, MinigameTile).moving) {
						return;
					}
				}
				FlxG.camera.flash();
				succes = true;
				scalingDown = true;
				won = false;
				return;
			}
			
			if (FlxG.keys.justPressed.LEFT) {
	
				moveHorizontaly(0, 1, 2, 3, -1);
				moveHorizontaly(0, 1, 2, 3, -1);
				moveHorizontaly(0, 1, 2, 3, -1);
			}
			if (FlxG.keys.justPressed.RIGHT) {
				moveHorizontaly(3, 2, 1, 0, 1);
				moveHorizontaly(3, 2, 1, 0, 1);
				moveHorizontaly(3, 2, 1, 0, 1);
			}
			if (FlxG.keys.justPressed.DOWN) {
				moveVertically(3, 2, 1, 0, 1);
				moveVertically(3, 2, 1, 0, 1);
				moveVertically(3, 2, 1, 0, 1);
			}
			if (FlxG.keys.justPressed.UP) {
				moveVertically(0, 1, 2, 3, -1);
				moveVertically(0, 1, 2, 3, -1);
				moveVertically(0, 1, 2, 3, -1);
			}
			if (FlxG.keys.justPressed.ESCAPE) {
				scalingDown = true;
			}
		}
	}
	
}