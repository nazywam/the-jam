package ;
import flixel.effects.particles.FlxEmitter;
import flixel.FlxSprite;
import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;
import flixel.FlxObject;
/**
 * ...
 * @author Michael
 */
class Actor extends FlxSprite {

	public var speed : Float = 100;
	public var jumpSpeed : Float = 230; //najniższy skok powinien przeskoczyć bullet wystrzelony przez potworka na tym samym poziomie
	public var bullets : FlxGroup;
	public var freezed : Bool = false;
	public var turnedRight : Bool;
	public var dead : Bool = false;
	public var gibs : FlxEmitter;
	public var hasWeapon : Bool = false;
	
	public var weaponPiercingPower : Int = 1;
	public var weaponRange : Int = 250;
	public var weaponCooldown : Float = 5;
	
	
	public var currentCooldown : Float = 0;
	
	public function new(x : Float, y : Float, turnedRight : Bool){
		super(x, y);
		
		bullets = new FlxGroup();
		
		gibs = new FlxEmitter();
		gibs.solid = true;
		gibs.acceleration.set(0, 100, 0, 300);
		gibs.drag.set(75, 0, 100, 10);
		gibs.angularDrag.set(250, 750);
		gibs.launchMode =  FlxEmitterMode.SQUARE;
		gibs.lifespan.set(1000, 1000);
		
		this.turnedRight = turnedRight;
		
		acceleration.y = 500;
		drag.x = 750;
	}
	public function fire() {
		if (currentCooldown <= 0) {
			
			currentCooldown = weaponCooldown;
			
			var b = new Bullet(x, y + 11, turnedRight, weaponRange, weaponPiercingPower);
			b.height = 3;
			b.offset.y = 14;
			b.velocity.x = 200;
			b.animation.play("default");
			if (turnedRight) {
				b.x += 7;
			}
			if (animation.name == "runWithWeapon") {
				b.y += 2;
			}
			if (!turnedRight) {
				b.velocity.x = -b.velocity.x;
			}
			bullets.add(b);
		}
	}
	public function distance(x1 : Float, y1 : Float, x2 : Float, y2 : Float) {
		return(Math.sqrt( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) ));
	}
	public function die() {
		solid = false;
		dead = true;
		
		//animation.play("die");
		acceleration.y = acceleration.x = 0;
		velocity.y = velocity.x = 0;
		
		gibs.setPosition(x + width / 2 , y + height / 2);
		gibs.start();
		
		visible = false;
	}
	override public function update(elapsed : Float) {
		currentCooldown = Math.max(0, currentCooldown - 0.5);
		
		for (q in bullets) {
			var b = cast(q, Bullet); 
			if (distance(b.x, b.y, b.startPos.x, b.startPos.y) > b.range || (b.animation.name == "hit" && b.animation.finished) ) {
				b.destroy();
				bullets.remove(b);
			}
			b.destroy();
		}
		if (!dead) {
			if (velocity.x != 0) {
				turnedRight = velocity.x > 0;
			}
			
			flipX = !turnedRight;
			
			if (freezed) {
				velocity.y = 0;
				velocity.x = 0;
				acceleration.x = 0;
				acceleration.y = 0;
				turnedRight = true;
			} else {
				acceleration.y = 500;
			}
		}
		super.update(elapsed);
	}
}
