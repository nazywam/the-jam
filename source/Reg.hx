package ;

import flixel.util.FlxSave;

/**
 * Handy, pre-built Registry class that can be used to store
 * references to objects and other things for quick-access. Feel
 * free to simply ignore it or change it in any way you like.
 */
class Reg
{
	public static var levels:Array<Dynamic> = [];
	
	public static var weaponUpgradeLevel : Int = 0;
	public static var weaponAmmo : Int = 10;
	
	public static var level : Int = 1;
	public static var world : Int = 2;
	
}