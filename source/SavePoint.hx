package ;
import flixel.FlxSprite;
/**
 * ...
 * @author Michael
 */
class SavePoint extends FlxSprite {

	public var currentlyUsed : Bool = false;
	
	public function new(posX : Float, posY : Float) {
		super(posX, posY);
		loadGraphic("assets/sprites/savePoints.png", true, 16, 18);
		animation.add("default", [0]);
		animation.add("used", [1]);
		animation.play("default");
		offset.y = 2;
	}
	
}
