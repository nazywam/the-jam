package ;

import flixel.FlxSprite;
import flixel.FlxState;
import openfl.Assets;
/**
 * ...
 * @author Michael
 */
class Animations extends FlxState
{
	override public function create() {
		super.create();
		
		var run = new FlxSprite(0, 0); 
		run.loadGraphic(Assets.getBitmapData("assets/sprites/player.png"), true, 26, 32);
		run.animation.add("run", [6, 7, 8, 9, 10], 12);
		run.animation.play("run");
		add(run);
	
		var runWithWeapon = new FlxSprite(52, 0);
		runWithWeapon.loadGraphic(Assets.getBitmapData("assets/sprites/player.png"), true, 26, 32);
		runWithWeapon.animation.add("runWithWeapon", [12, 13, 14, 15, 16, 17], 12);
		runWithWeapon.animation.play("runWithWeapon");
		add(runWithWeapon);
		
		var wink = new FlxSprite(104, 0);
		wink.loadGraphic(Assets.getBitmapData("assets/sprites/player.png"), true, 26, 32);
		wink.animation.add("wink", [2, 18, 19, 20], 2);
		wink.animation.play("wink");
		add(wink);
		
		var scratch = new FlxSprite(104 + 52, 0);
		scratch.loadGraphic(Assets.getBitmapData("assets/sprites/player.png"), true, 26, 32);
		scratch.animation.add("scratch", [0, 24, 25, 26, 27, 26,27,26, 25, 24], 5);
		scratch.animation.play("scratch");
		add(scratch);
		
		var playerBullet = new FlxSprite(208, 0);
		playerBullet.loadGraphic(Assets.getBitmapData("assets/sprites/bullets.png"), true, 9, 20);
		playerBullet.animation.add("fly", [0, 1, 2, 3, 4, 5, 6, 7, 8], 7);
		playerBullet.animation.play("fly");
		add(playerBullet);
		
		var otherBullet = new FlxSprite(208 + 52, 0);
		otherBullet.loadGraphic(Assets.getBitmapData("assets/sprites/bullets.png"), true, 9, 20);
		otherBullet.animation.add("fly", [9, 10, 11, 12, 13, 14, 15, 16, 17], 7);
		otherBullet.animation.play("fly");
		add(otherBullet);
		
		var enemyRun = new FlxSprite(208 + 52 * 2, 0);
		enemyRun.loadGraphic(Assets.getBitmapData("assets/sprites/enemy.png"), true, 26, 33);
		enemyRun.animation.add("run", [6, 7, 8, 9], 12);
		enemyRun.animation.play("run");
		add(enemyRun);
		
		var enemyPunch = new FlxSprite(208 + 52 * 3, 0);
		enemyPunch.loadGraphic(Assets.getBitmapData("assets/sprites/enemy.png"), true, 26, 33);
		enemyPunch.animation.add("run", [12, 13, 14, 15, 16, 17, 17, 17, 17, 17, 17, 17, 17], 15);
		enemyPunch.animation.play("run");
		add(enemyPunch);
		
		var playerJump = new FlxSprite(0, 40);
		playerJump.loadGraphic("assets/sprites/player.png", true, 26, 32);
		playerJump.animation.add("jump", [36, 37, 38, 39, 40, 41], 6);
		playerJump.animation.play("jump");
		add(playerJump);
		
	}
}