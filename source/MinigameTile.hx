package ;
import flixel.FlxSprite;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Michael
 */
class MinigameTile extends FlxSprite {
	
	public var gridX : Int;
	public var gridY : Int;
	public var type : BlockType;
	public var destination : FlxPoint;
	public var moving : Bool = false;
	
	public function new(posX : Float, posY : Float, gX : Int, gY : Int, type : BlockType) {
		super(posX, posY);
		destination = new FlxPoint(posX, posY);
		loadGraphic("assets/sprites/minigame/blocks.png", false, 48, 48);
		this.gridX = gX;
		this.gridY = gY;
		this.type = type;
		
		switch(type) {
			case BlockType.Block:
				animation.add("default", [0]);
			case BlockType.Key:
				animation.add("default", [1]);
			case BlockType.Chest:
				animation.add("default", [2]);
			case BlockType.Solid:
				animation.add("default", [3]);
			case BlockType.Error:
				animation.add("default", [4]);
		}
		animation.play("default");
	}
	override public function update(e : Float) {
		super.update(e);
		moving = false;
		if (Math.round(destination.x) != Math.round(x)) {
			x += (destination.x - x) / 5;
			moving = true;
		}
		if (Math.round(destination.y) != Math.round(y)) {
			y += (destination.y - y) / 5;
			moving = true;
		}
	}
}

enum BlockType{
	Key;
	Chest;
	Block;
	Solid;
	Error;
}