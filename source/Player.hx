package ;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.input.gamepad.XboxButtonID;
import flixel.input.gamepad.FlxGamepad;
import flixel.FlxObject;
/**
 * ...
 * @author Michael
 */
class Player extends Actor {


	public var weaponUpgradeProgress : Int = 1;
	
	public function new(x:Float, y:Float) {
		super(x, y, false);
		
		hasWeapon = true;
		
		loadGraphic("assets/sprites/player.png", true, 26, 32);
		animation.add("front", [0]);
		animation.add("stand", [1]);
		animation.add("standWithWeapon", [2]);
		animation.add("run", [6, 7, 8, 9, 10], 12);
		animation.add("runWithWeapon", [12, 13, 14, 15, 16, 17], 12);
		animation.add("teleportOut", [30, 31, 32, 33, 5], 8, false);
		animation.add("teleportIn", [5, 33, 32, 31, 30, 0], 8, false);
		animation.add("jump", [37], 6, false);
		animation.add("fall", [38, 39], 12);
		
		gibs.loadParticles("assets/sprites/playerGibs.png", 36, 16, true);
		gibs.makeParticles(6, 6);
		
		width = 12;
		offset.x = 7;
	}
	
	override public function update(elapsed : Float) {
		if (!dead && !freezed) {
			if (FlxG.keys.pressed.LEFT) {
				velocity.x = -speed;
			}
			if (FlxG.keys.pressed.RIGHT) {
				velocity.x = speed;
			}
			
			if (FlxG.keys.justPressed.UP && isTouching(FlxObject.FLOOR) ) {
				velocity.y = -jumpSpeed;
			}
			if (!dead && !freezed) {
				if (velocity.y < 0) {
					animation.play("jump");
				} else if (velocity.y > 0) {
					animation.play("fall");
				} else if (velocity.x != 0) {
					if (hasWeapon) {
						animation.play("runWithWeapon");	
					} else {
						animation.play("run");	
					}
				} else {
					if (hasWeapon) {
						animation.play("standWithWeapon");
					} else {
						animation.play("stand");
					}
				}
			}
			
		}
		super.update(elapsed);
	}
}
