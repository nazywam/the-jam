package ;
import flixel.FlxSprite;

/**
 * ...
 * @author Michael
 */
class Platform extends FlxSprite{

	public var moveType : MoveType;
	public var moveDirection = 1;
	public var startedBy : Int;
	public var moving : Bool = false;
	
	public function new(posX : Float, posY : Float, moveType : MoveType, started : Int) {
		super(posX, posY);
		this.moveType = moveType;
		startedBy = started;
		loadGraphic("assets/sprites/platform.png", false, 48, 16);
		immovable = true;
		
		if (startedBy == -1) {
			moving = true;
		}
	}
		
	override public function update(e : Float) {
		super.update(e);
		if (moving) {
			switch moveType {
				case Static:
				case Horizontal:
					x += .75 * moveDirection;
				case Vertical:
					if (moveDirection == 1) {
						y += .75;
					} else {
						y -= .75;
					}
					
			}
		}
	}
}

enum MoveType {
	Static;
	Horizontal;
	Vertical;
}