package ;
import flixel.FlxSprite;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Michael
 */
class Bullet extends FlxSprite {
	public var startPos : FlxPoint;
	public var type : Int = 1;
	public var range : Float;
	public var piericingPower : Int;
	
	public function new(x:Float, y:Float, turnedRight : Bool, range : Int,  p : Int) {
		super(x, y);
		init(x, y, turnedRight,range,  p);
	}
	public function init(x : Float, y : Float, turnedRight : Bool,range : Int, p : Int) {
		startPos = new FlxPoint(x, y);
		piericingPower = p;
		this.range = range;
		
		loadGraphic("assets/sprites/bullets.png", true, 9, 20);
		
		var i = 9 * (type-1);
		
		animation.add("default", [0 + i], 10);
		animation.add("hit", [2 + i, 3 + i, 4 + i, 5 + i, 6 + i, 7 + i, 8 + i],  20, false);
		animation.play("default");
		
		flipX = !turnedRight;
	}
	override public function update(elapsed : Float) {
		super.update(elapsed);
	}
	override public function destroy() {
		startPos.destroy();
	}
}