package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import openfl.Assets;
/**
 * ...
 * @author Michael
 */
class Enemy extends Actor
{
	public var isPunching : Bool = false;
	public var isShooting : Bool = false;
	
	
	public function new(x:Float, y:Float, hasWeapon : Bool) {
		super(x, y, turnedRight);
		
		this.hasWeapon = hasWeapon;
		weaponCooldown = 45;
		
		gibs.loadParticles("assets/sprites/enemyGibs.png", 36, 16, true);
		gibs.makeParticles(6,6);
		
		loadGraphic("assets/sprites/enemy.png", false, 26, 33);
		animation.add("front", [1]);
		animation.add("run", [6, 7, 8, 9], 12);
		animation.add("runWithWeapon", [18, 19, 20, 21], 12);
		animation.add("punch", [12,14,15,15,16,17], 10, false);

		height = 32;
		speed -= 50;
		
		width = 12;
		offset.x = 7;
	}
	override public function update(elapsed : Float) {
		if (!freezed && !dead) {
			if (isPunching) {
			}
			else {
				if (velocity.x != 0) {
					if (hasWeapon) {
						animation.play("runWithWeapon");	
					} else {
						animation.play("run");	
					}
				} else {
					animation.play("front");
				}
			}
		}
		
		super.update(elapsed);
	}
}