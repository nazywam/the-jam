package ;

import flixel.FlxSprite;
/**
 * ...
 * @author Michael
 */
class Spikes extends FlxSprite {

	public var type : SpikeType;
	
	public function new(X:Float=0, Y:Float=0, t : SpikeType){
		super(X, Y);
		
		type = t;
		
		if (type == SpikeType.Brick) {
			loadGraphic("assets/sprites/objects.png", false, 16, 16);
			animation.add("default", [4]);
			animation.play("default");
			height = 7;
			y += 8;
			offset.y = 8;
		} else if (type == SpikeType.Round) {
			loadGraphic("assets/sprites/roundSpike.png");
			
			width = height = 10;
			offset.x = offset.y = 3;
		}
		
		
		
	}
	
}

enum SpikeType {
	Brick;
	Round;
}